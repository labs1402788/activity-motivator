# README #

### Project idea & summary ###


#### Activity Motivator with Polar H6 heart rate monitor
Uses any BLE Heart rate monitor

Notifies you when you are walking too lazily

Helps you manage your interval workout

Ensures you won’t slack off during a workout

### Features ###

* Bluetooth LE is used to get heart rate signal from a wearable monitor
* Uses Google Api Client’s GPS to get your speed outdoors
* Plays chime sound according to your selected mode to help you stay active.

### Functionality ###

Walking: If you’re walking and your heart rate is under X bpm -> Motivator Chime

Interval workout: Motivator Chimes to indicate the starts and ends of interval Bursts

Free workout: Once your heart rate rises over X, You’ll get a Motivator Chime if your heart rate drops under Y.

### Design ###

Color scheme created with coolors.co
Tablayout &  swipeable Fragments
As simple as possible
No hamburgers
No cluttered setting menus behind buttons


## Installation ##
Clone the master branch and open on Android Studio.


## License ##

Created by Eemeli Heinonen and Mortti Aittokoski